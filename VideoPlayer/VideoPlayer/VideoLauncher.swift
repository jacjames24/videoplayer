//
//  VideoLauncher.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/24.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class VideoLauncher: NSObject {
    var videoView: UIView?
    var mainViewController: MainViewController?
    var videoPlayer: VideoPlayer?
    var currentState: StateOfPlayerView?

    enum StateOfPlayerView {
        case minimized
        case fullScreen
    }

    func showVideoPlayer(video: Video?) {
        if let keyWindow = UIApplication.shared.keyWindow {
            videoView = UIView(frame: keyWindow.frame)
            videoView?.backgroundColor = UIColor.black
            videoView?.frame = keyWindow.frame

            videoPlayer = VideoPlayer(frame: keyWindow.frame)
            videoPlayer?.video = video
            videoPlayer?.launcher = self

            if let player = videoPlayer {
                videoView?.addSubview(player)
                currentState = StateOfPlayerView.fullScreen
            }

            if let view = videoView {
                keyWindow.addSubview(view)
            }

        }
    }

    // there are two triggers for this:
    // 1. when back is pressed
    // 2. when user selects another video while player view is minimized
    // that is why there are redundancy checks for pausing

    func hideVideoPlayer() {
        videoView?.removeFromSuperview()

        if (videoPlayer?.isPlaying)! {
            videoPlayer?.player?.pause()
        }

        //function to handle handling of hiding of video player
        if mainViewController != nil {
            mainViewController?.onCloseVideoPlayer()
        }
    }

    func minimizeVideoPlayerTrigger() {
        self.currentState = StateOfPlayerView.minimized

        if mainViewController != nil {
            mainViewController?.onMinimizeVideoPlayer()
        }
    }

    func maximizeVideoPlayerTrigger() {
        self.currentState = StateOfPlayerView.fullScreen

        if mainViewController != nil {
            mainViewController?.onSetToFullScreenVideoPlayer()
        }
    }

    func minimizeVideoPlayer() {

        if let keyWindow = UIApplication.shared.keyWindow {

            let xPos = keyWindow.frame.width - (keyWindow.frame.width/3 * 2)
            let yPos = keyWindow.frame.height - (keyWindow.frame.height / 4)

            UIView.animate(withDuration: 0.3, animations: {
                if self.videoView != nil {
                    let width = keyWindow.frame.width/3 * 2
                    self.videoView?.frame = CGRect(x: xPos, y: yPos,
                                                   width: width ,
                                                   height: (width * 9 / 16))
                }
            }, completion: {(_) in
                self.videoPlayer?.minimizePlayerLayer()
                self.videoPlayer?.backgroundColor = UIColor.clear
            })
        }
    }

    func maximizeVideoPlayer() {
        if let keyWindow = UIApplication.shared.keyWindow {
            self.videoView?.frame = keyWindow.frame
            self.videoPlayer?.maximizePlayerLayer()
            self.videoPlayer?.backgroundColor = UIColor.black
        }
    }

}
