//
//  VideoCell.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/23.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {
    var video: Video? {
        didSet {

            if let titleText = video?.title {
                if let presenterText = video?.presenter {
                    titleAndPresenterLabel.text = titleText + " by " + presenterText
                }
            }

            setupThumbnailImage()

            if let descriptionText = video?.descriptionText {
                descriptionTextView.text = descriptionText
            }

            if let duration = video?.duration {
                durationLabel.textFormattedToMinAndSec(durationInMs: duration)
            }
        }
    }

    func setupThumbnailImage() {
        if let thumbnailImageURL = video?.thumbnailImageName {
            thumnailImageView.loadImageUsingUrlString(urlString: thumbnailImageURL)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    let thumnailImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.image = UIImage.init(named: "placeholder")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    let durationContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0.50)
        return view
    }()

    let durationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00:00"
        label.textColor = .white
        label.font = label.font.withSize(13)
        label.textAlignment = .right
        return label
    }()

    let titleAndPresenterLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()

    let descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: -4, bottom: 0, right: 0)
        textView.textColor = UIColor.gray

        return textView
    }()

    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 230 / 255, green: 230 / 255, blue: 230 / 255, alpha: 1)

        return view
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupViews() {
        addSubview(thumnailImageView)

        thumnailImageView.addSubview(durationContainerView)
        durationContainerView.rightAnchor.constraint(
            equalTo: thumnailImageView.rightAnchor,
            constant: -8).isActive = true
        durationContainerView.bottomAnchor.constraint(
            equalTo: thumnailImageView.bottomAnchor,
            constant: -10).isActive = true
        durationContainerView.widthAnchor.constraint(equalToConstant: 45).isActive = true
        durationContainerView.heightAnchor.constraint(equalToConstant: 20).isActive = true

        durationContainerView.addSubview(durationLabel)
        durationLabel.centerXAnchor.constraint(
            equalTo: durationContainerView.centerXAnchor,
            constant: -4).isActive = true
        durationLabel.centerYAnchor.constraint(equalTo: durationContainerView.centerYAnchor).isActive = true
        durationLabel.widthAnchor.constraint(equalToConstant: 45).isActive = true
        durationLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true

        addSubview(separatorView)
        addSubview(titleAndPresenterLabel)
        addSubview(descriptionTextView)

        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: thumnailImageView)

        addConstraintsWithFormat(format: "V:|-16-[v0]-8-[v1(40)]-4-[v2(30)]-[v3(1)]|",
            views: thumnailImageView, titleAndPresenterLabel, descriptionTextView, separatorView)

        addConstraintsWithFormat(format: "H:|[v0]|", views: separatorView)

        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: titleAndPresenterLabel)

        addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: descriptionTextView)

    }
}
