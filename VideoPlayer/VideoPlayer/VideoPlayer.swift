//
//  VideoPlayer.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/26.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayer: UIView, UIGestureRecognizerDelegate {

    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var isPlaying = false
    var urlString: String?
    var launcher: VideoLauncher?
    var initialToggle: Bool?
    var timerForHiding: Timer?

    let hideControlAfter = 5.0

    var video: Video? {
        didSet {
            self.urlString = video?.urlString
            self.titleLabel.text = video?.title
            self.initialToggle = true

            setupPlayerView()
            setupControlsContainerView()
        }
    }

    let topContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0.40)
        return view
    }()

    let bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0.40)
        return view
    }()

    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.buttonWithImage(image: "back")
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return button
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()

    let minimizeButton: UIButton = {
        let button = UIButton(type: .system)
        button.buttonWithImage(image: "minimize")
        button.addTarget(self, action: #selector(handleMinimize), for: .touchUpInside)
        return button
    }()

    lazy var controlsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 1)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleControlsContainer))
        tapGestureRecognizer.delegate = self
        view.addGestureRecognizer(tapGestureRecognizer)
        return view
    }()

    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .whiteLarge)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.startAnimating()
        return aiv
    }()

    lazy var pausePlayButton: UIButton = {
        let button = UIButton(type: .system)
        button.buttonWithImage(image: "pause")
        button.addTarget(self, action: #selector(handlePause), for: .touchUpInside)
        button.alpha = 0.0
        return button
    }()

    let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00:00"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()

    let videoLengthLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "00:00"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textAlignment = .right
        return label
    }()

    lazy var videoSlider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumTrackTintColor = .red
        slider.maximumTrackTintColor = .white
        slider.setThumbImage(UIImage(named: "thumb"), for: UIControl.State())

        slider.addTarget(self, action: #selector(handleSliderChange), for: .valueChanged)

        return slider
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupPlayerView() {

        backgroundColor = UIColor.black

        if let urlToPlay = urlString {
            if let url = URL(string: urlToPlay) {

                player = AVPlayer(url: url)

                NotificationCenter.default.addObserver(
                    self, selector: #selector(playerDidFinishPlaying),
                    name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                    object: player?.currentItem)

                playerLayer = AVPlayerLayer(player: player)
                if let thisLayer = playerLayer {
                    self.layer.addSublayer(thisLayer)
                }
                playerLayer?.frame = self.frame

                player?.play()
                isPlaying = true

                //this part is used for checking if loading has thus ended
                player?.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)

                //this part is for updating the slider values
                let interval = CMTime(value: 1, timescale: 2)
                player?.addPeriodicTimeObserver(forInterval: interval,
                    queue: DispatchQueue.main,
                    using: { (progressTime) in

                        let seconds = CMTimeGetSeconds(progressTime)
                        let secondsString = String(format: "%02d", Int(seconds.truncatingRemainder(dividingBy: 60)))
                        let minutesString = String(format: "%02d", Int(seconds / 60))

                        self.currentTimeLabel.text = "\(minutesString):\(secondsString)"

                        if let duration = self.player?.currentItem?.duration {
                            let durationSeconds = CMTimeGetSeconds(duration)
                            self.videoSlider.value = Float(seconds / durationSeconds)
                        }
                    })
            }
        }
    }

    @objc func playerDidFinishPlaying() {
        print("Video Finished")

        isPlaying = false
        self.toggleAllComponents(toHide: false)

        timerForHiding?.invalidate()

        //if the player is playing, we will hide the controls
        timerForHiding?.invalidate()
        timerForHiding = Timer.scheduledTimer(timeInterval: 2.0,
            target: self, selector: #selector(handleBack),
            userInfo: nil, repeats: false)
    }

    func setupControlsContainerView() {
        controlsContainerView.frame = frame
        addSubview(controlsContainerView)

        setupTopControl()
        setupPlayAndActIndicatorControls()
        setupBottomControl()
    }

    func setupTopControl() {
        //top container view
        controlsContainerView.addSubview(topContainerView)
        addConstraintsWithFormat(format: "H:|[v0]|", views: topContainerView)
        addConstraintsWithFormat(format: "V:|[v0(50)]", views: topContainerView)

        //back button
        controlsContainerView.addSubview(backButton)
        backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        backButton.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 32).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 32).isActive = true

        //title label
        controlsContainerView.addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: backButton.rightAnchor, constant: 10).isActive = true
        titleLabel.topAnchor.constraint(equalTo: backButton.topAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 32).isActive = true

        //minimize button
        controlsContainerView.addSubview(minimizeButton)
        minimizeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        minimizeButton.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        minimizeButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        minimizeButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
    }

    func setupPlayAndActIndicatorControls() {
        //activity view indicator
        controlsContainerView.addSubview(activityIndicatorView)
        activityIndicatorView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        //play pause button
        controlsContainerView.addSubview(pausePlayButton)
        pausePlayButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        pausePlayButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        pausePlayButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        pausePlayButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

    func setupBottomControl() {

        //bottom container view
        controlsContainerView.addSubview(bottomContainerView)
        addConstraintsWithFormat(format: "H:|[v0]|", views: bottomContainerView)
        addConstraintsWithFormat(format: "V:[v0(40)]|", views: bottomContainerView)

        //current time label
        controlsContainerView.addSubview(currentTimeLabel)
        currentTimeLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        currentTimeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        currentTimeLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        currentTimeLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true

        //video length label
        controlsContainerView.addSubview(videoLengthLabel)
        videoLengthLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        videoLengthLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        videoLengthLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        videoLengthLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true

        //video slider
        controlsContainerView.addSubview(videoSlider)
        videoSlider.rightAnchor.constraint(equalTo: videoLengthLabel.leftAnchor).isActive = true
        videoSlider.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        videoSlider.leftAnchor.constraint(equalTo: currentTimeLabel.rightAnchor).isActive = true
        videoSlider.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "currentItem.loadedTimeRanges" {
            activityIndicatorView.stopAnimating()
            controlsContainerView.backgroundColor = UIColor.clear

            if initialToggle == true {
                pausePlayButton.alpha = 1.0

                //if the player is playing, we will hide the controls
                timerForHiding = Timer.scheduledTimer(timeInterval: hideControlAfter,
                    target: self, selector: #selector(hideAllComponentsWithAnimation),
                    userInfo: nil, repeats: false)

                initialToggle = false
            }

            if let duration = player?.currentItem?.duration {
                let seconds = CMTimeGetSeconds(duration)

                let secondsText = Int(seconds) % 60
                let minutesText = String(format: "%02d", Int(seconds) / 60)
                videoLengthLabel.text = "\(minutesText):\(secondsText)"
            }

        }
    }

    @objc func handlePause() {
        if isPlaying {
            player?.pause()
            pausePlayButton.setImage(UIImage(named: "play"), for: UIControl.State())
            self.toggleAllComponents(toHide: false)
            timerForHiding?.invalidate()
        } else {
            player?.play()
            pausePlayButton.setImage(UIImage(named: "pause"), for: UIControl.State())

            // if the player is playing, we will hide the controls
            timerForHiding?.invalidate()
            timerForHiding = Timer.scheduledTimer(timeInterval: hideControlAfter,
                target: self, selector: #selector(hideAllComponentsWithAnimation),
                userInfo: nil, repeats: false)
        }

        isPlaying = !isPlaying
    }

    @objc func handleBack() {
        if isPlaying {
            player?.pause()
        }

        if let vlauncher = launcher {
            vlauncher.hideVideoPlayer()
        }
    }

    // note to self:
    // order of calls
    // 1. handle functions
    // 2. launcher triggers
    // 3. maincontroller functions
    // 4. callback in launcher
    // 5. callback in players

    // callback for the minimize button
    @objc func handleMinimize() {
        print("this is for the minimize")

        toggleAllComponents(toHide: true)

        if let vlauncher = launcher {
            vlauncher.minimizeVideoPlayerTrigger()
        }
    }

    // callback for the maximize button
    func handleToFullScreen() {

        toggleAllComponents(toHide: false)

        if let vlauncher = launcher {
            vlauncher.maximizeVideoPlayerTrigger()
        }
    }

    // callback from Launcher
    func minimizePlayerLayer() {
        if let keyWindow = UIApplication.shared.keyWindow {
            let width = keyWindow.frame.width / 3 * 2

            playerLayer?.frame = CGRect(x: 0, y: 0,
                width: width, height: (width * 9 / 16))

        }
    }

    // callback from Launcher
    func maximizePlayerLayer() {
        if let keyWindow = UIApplication.shared.keyWindow {
            playerLayer?.frame = CGRect(x: 0, y: 0,
                width: keyWindow.frame.width, height: keyWindow.frame.height)
        }
    }

    @objc func handleSliderChange() {

        if let duration = player?.currentItem?.duration {

            timerForHiding?.invalidate()

            let totalSeconds = CMTimeGetSeconds(duration)

            let value = Float64(videoSlider.value) * totalSeconds

            let seekTime = CMTime(value: Int64(value), timescale: 1)

            player?.seek(to: seekTime, completionHandler: { (_) in

            })
        }
    }

    @objc func toggleControlsContainer() {
        if launcher?.currentState == VideoLauncher.StateOfPlayerView.fullScreen {
            print("full screen state: toggle the controls")
            toggleAllComponents(toHide: titleLabel.alpha == 0.0 ? false : true)

            // then if it is playing, then we will hide the controls
            if isPlaying {
                timerForHiding?.invalidate()
                timerForHiding = Timer.scheduledTimer(timeInterval: hideControlAfter,
                    target: self, selector: #selector(hideAllComponentsWithAnimation),
                    userInfo: nil, repeats: false)
            }
        } else {
            // maximize the view...
            print("minimized screen state: will maximize the view")
            handleToFullScreen()

            // if the player is playing, we will hide the controls
            timerForHiding?.invalidate()
            timerForHiding = Timer.scheduledTimer(timeInterval: hideControlAfter,
                target: self, selector: #selector(hideAllComponentsWithAnimation),
                userInfo: nil, repeats: false)
        }
    }

    // is called by the timers when we want hide the components
    @objc func hideAllComponentsWithAnimation() {
        self.toggleAllComponents(toHide: true)
    }

    // generic function for showing/hiding of the components
    func toggleAllComponents(toHide: Bool) {
        let alpha: CGFloat = toHide == true ? 0.0 : 1.0

        UIView.animate(withDuration: 0.3, animations: {
            self.topContainerView.alpha = alpha
            self.bottomContainerView.alpha = alpha
            self.backButton.alpha = alpha
            self.titleLabel.alpha = alpha
            self.minimizeButton.alpha = alpha
            self.pausePlayButton.alpha = alpha
            self.currentTimeLabel.alpha = alpha
            self.videoLengthLabel.alpha = alpha
            self.videoSlider.alpha = alpha
        })
    }
}
