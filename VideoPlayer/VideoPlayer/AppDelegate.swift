//
//  AppDelegate.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/20.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.all

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Override point for customization after application launch.

        let window = UIWindow(frame: UIScreen.main.bounds)

        let layout = UICollectionViewFlowLayout()
        window.rootViewController = UINavigationController(
            rootViewController: MainViewController(collectionViewLayout: layout))

        window.makeKeyAndVisible()
        self.window = window

        return true
    }

    func application(
        _ application: UIApplication,
        supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) { }

}
