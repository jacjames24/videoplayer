//
//  MainViewController.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/20.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class MainViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var willLaunchVideoPlayer = false

    var videos: [Video]?

    var selectedVideo: Video?

    var videoLauncher: VideoLauncher?

    func fetchVideos() {
        ApiService.sharedInstance.fetchVideos { (videos: [Video]) in

            self.videos = videos

            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchVideos()

        navigationItem.title = "Videos"
        navigationController?.hidesBarsOnSwipe = true

        let topBar = UIView(frame: UIApplication.shared.statusBarFrame)
        topBar.backgroundColor = UIColor(red: 230/255, green: 32/255, blue: 31/255, alpha: 1)
        view.addSubview(topBar)

        collectionView?.backgroundColor = UIColor.white

        collectionView?.register(VideoCell.self, forCellWithReuseIdentifier: "cellId")

        //This is for handling the rotation when app becomes active
        //For the scenario wherein user was playing a video, and then app goes to the background
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onRestorePreviousState), name: UIApplication.didBecomeActiveNotification, object: nil)

        AppUtility.lockOrientation(.portrait)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.barTintColor = UIColor(red: 230/255, green: 32/255, blue: 31/255, alpha: 1)
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary(
            [NSAttributedString.Key.foregroundColor.rawValue: UIColor.white])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as? VideoCell
        cell?.video = videos?[indexPath.item]
        return cell!
    }

    // will display cell using the 16 * 9 aspect ratio
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = (view.frame.width - 16 - 16) * 9/16
        return CGSize.init(width: view.frame.width, height: height + 98)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("we have selected an item")

        AppUtility.lockOrientation(UIInterfaceOrientationMask.all)

        //we will need to make sure that we remove any existing instances.
        if self.videoLauncher != nil {
            self.videoLauncher?.hideVideoPlayer()
        }

        selectedVideo = videos?[indexPath.item]
        willLaunchVideoPlayer = true
        onOpenVideoPlayer()
    }

    func launchVideoPlayer() {
        videoLauncher = VideoLauncher()
        videoLauncher?.mainViewController = self
        videoLauncher?.showVideoPlayer(video: selectedVideo)
    }

    //callback from Video Launcher in order for main view to know what
    //it will do. In this instance, the mainviewcontroller will need to 
    //change the orientation back to portrait mode

    func onCloseVideoPlayer() {
        willLaunchVideoPlayer = false
        self.videoLauncher = nil

        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }

    func onOpenVideoPlayer() {
        if willLaunchVideoPlayer {
            AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
        }
    }

    @objc func onRestorePreviousState() {
        if self.videoLauncher?.currentState == VideoLauncher.StateOfPlayerView.minimized {
            //we will actually do nothing for now...
        } else if self.videoLauncher?.currentState == VideoLauncher.StateOfPlayerView.fullScreen {
            onOpenVideoPlayer()
        }
    }

    func onMinimizeVideoPlayer() {
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }

    func onSetToFullScreenVideoPlayer() {
        AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { (_) in
            if self.willLaunchVideoPlayer {
                if self.videoLauncher == nil {
                    self.launchVideoPlayer()
                } else {
                    if self.videoLauncher?.currentState == VideoLauncher.StateOfPlayerView.fullScreen {
                        self.videoLauncher?.maximizeVideoPlayer()
                    }
                }
            }
        }, completion: { _ in
            print("did finish rotation")
            if self.videoLauncher?.currentState == VideoLauncher.StateOfPlayerView.minimized {
                self.videoLauncher?.minimizeVideoPlayer()
            }
        })
    }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToOptionalNSAttributedStringKeyDictionary(
    _ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
