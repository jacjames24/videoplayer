//
//  ApiService.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/24.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class ApiService: NSObject {

    let feedURL = "https://bit.ly/2NVwfhp"

    static let sharedInstance = ApiService()

    func fetchVideos(completion: @escaping ([Video]) -> Void) {
        let url = URL.init(string: feedURL)

        URLSession.shared.dataTask(with: url!) { (data, _, error) in
            if error != nil {
                print(error ?? "There is an error encountered during the processing of this request.")
                return
            }

            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)

                guard let dictionaries = json as? [[String: Any]] else { return }

                var videos = [Video]()

                for dictionary in dictionaries {
                    let video = Video()
                    video.title = dictionary["title"] as? String
                    video.descriptionText = dictionary["description"] as? String
                    video.presenter = dictionary["presenter_name"] as? String
                    video.thumbnailImageName = dictionary["thumbnail_url"] as? String
                    video.duration = dictionary["video_duration"] as? NSNumber
                    video.urlString = dictionary["video_url"] as? String

                    videos.append(video)
                }

                completion(videos)

            } catch let jsonError {
                print("Error encountered during serialization: \(jsonError)")
            }

            }.resume()
    }
}
