# Video Player's README

This is a video player that loads a feed from a web service and loads the feed into a collection view.

Available functions are the following:

- load video feed from a specified web service
- play a video by selecting an item from the collection view
- play/pause/scrub via the playback controls
- asynchronous loading of thumbnails
- caching of image thumbnails
- picture in picture playing (video is displayed in a minimized window)

The user interface of the application is implemented in-code via layoutconstraints.

The app is implemented using *Swift 3.0*