//
//  Video.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/23.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

class Video: NSObject {
    var thumbnailImageName: String?
    var title: String?
    var presenter: String?
    var descriptionText: String?
    var duration: NSNumber?
    var urlString: String?
}
