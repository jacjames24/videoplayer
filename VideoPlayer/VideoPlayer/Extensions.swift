//
//  Extensions.swift
//  VideoPlayer
//
//  Created by Jacquiline Guzman on 2017/09/23.
//  Copyright © 2017 jjrg.com. All rights reserved.
//

import UIKit

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()

        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }

        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format,
            options: NSLayoutConstraint.FormatOptions(),
            metrics: nil,
            views: viewsDictionary))
    }
}

extension UILabel {
    func textFormattedToMinAndSec(durationInMs: NSNumber) {
        let milliseconds = durationInMs.doubleValue
        var seconds = milliseconds / 1000
        var minutes = seconds / 60
        seconds = seconds.truncatingRemainder(dividingBy: 60)
        minutes = minutes.truncatingRemainder(dividingBy: 60)

        self.text = "\(Int(minutes)):\(Int(seconds))"
    }
}

extension UIButton {
    func buttonWithImage(image: String) {
        let image = UIImage(named: image)
        self.setImage(image, for: UIControl.State())
        self.translatesAutoresizingMaskIntoConstraints = false
        self.tintColor = .white
    }
}

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {

    var imageUrlString: String?

    func loadImageUsingUrlString(urlString: String) {

        imageUrlString = urlString

        let imageURL = URL(string: urlString)

        image = nil

        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }

        URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, _, error) in

            if error != nil {
                print("Error encountered during downloading of image")
                return
            }

            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)

                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }

                imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)

            }

        }).resume()
    }
}

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask,
                                andRotateTo rotateOrientation: UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }

}
